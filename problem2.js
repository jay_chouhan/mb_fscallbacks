/*
    Problem 2:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Read the given file lipsum.txt
        2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
        3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
        5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
*/

const fs = require("fs");

const problem2 = (fileName, callback) => {
  fs.readFile(fileName, "utf-8", (err, data) => {
    if (err) {
      callback(new Error(`Error reading ${fileName}`));
    } else {
      const textInUpperCase = data.toUpperCase();
      const fileForUpperCase = "upperCase.txt";

      fs.writeFile(fileForUpperCase, textInUpperCase, (err) => {
        if (err) {
          callback(new Error(`Error writing ${fileForUpperCase}`, null));
        } else {
          console.log(`${fileForUpperCase} created`);

          const filenames = "filenames.txt";

          fs.writeFile(filenames, fileForUpperCase, (err) => {
            if (err) {
              callback(new Error(`Error writing in ${filenames}`, null));
            } else {
              console.log(`${fileForUpperCase} added to ${filenames}`);

              fs.readFile(fileForUpperCase, "utf-8", (err, data) => {
                if (err) {
                  callback(new Error(`Error reading ${fileForUpperCase}`, null));
                } else {
                  const textInLowerCase = data.toLowerCase().split(". ").join(".\n");

                  const fileForLowerCase = "lowerCase.txt";

                  fs.writeFile(fileForLowerCase, textInLowerCase, (err) => {
                    if (err) {
                      callback(new Error(`Error writing ${fileForLowerCase}`, null));
                    } else {
                      console.log(`${fileForLowerCase} created`);

                      fs.appendFile(filenames, `\n${fileForLowerCase}`, (err) => {
                        if (err) {
                          callback(new Error(`Error writing in ${filenames}`, null));
                        } else {
                          console.log(`${fileForLowerCase} added to ${filenames}`);

                          fs.readFile(fileForLowerCase, "utf-8", (err, data) => {
                            if (err) {
                              callback(new Error(`Error reading ${fileForLowerCase}`, null));
                            } else {
                              const sortedText = data
                                .split("\n")
                                .sort((a, b) => {
                                  if (a < b) {
                                    return -1;
                                  }
                                })
                                .join("\n");

                              const fileForSortedText = "sortedText.txt";

                              fs.writeFile(fileForSortedText, sortedText, (err) => {
                                if (err) {
                                  callback(new Error(`Error writing ${fileForSortedText}`, null));
                                } else {
                                  console.log(`${fileForSortedText} created`);

                                  fs.appendFile(filenames, `\n${fileForSortedText}`, (err) => {
                                    if (err) {
                                      callback(new Error(`Error writing in ${filenames}`, null));
                                    } else {
                                      console.log(`${fileForSortedText} added to ${filenames}`);

                                      fs.readFile(filenames, "utf-8", (err, data) => {
                                        if (err) {
                                          callback(new Error(`Error reading ${filenames}`, null));
                                        } else {
                                          const fileNamesData = data.split("\n");

                                          fileNamesData.map((file) => {
                                            fs.unlink(file, (err) => {
                                              if (err) {
                                                console.error(err);
                                              } else {
                                                console.log(`${file} deleted succesfully`);
                                              }
                                            });
                                          });
                                        }
                                      });
                                    }
                                  });
                                }
                              });
                            }
                          });
                        }
                      });
                    }
                  });
                }
              });
            }
          });
        }
      });
    }
  });
};

module.exports = problem2;
