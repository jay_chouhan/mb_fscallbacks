const fs = require("fs");
const path = require("path");

const problem1 = require("../problem1");

const directory = path.join(__dirname, "../", "randomFiles");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  }
  console.log(data);
};

problem1(directory, callback);
