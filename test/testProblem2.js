const problem2 = require("../problem2");

const path = require("path");

const fileName = path.join(__dirname, "../", "lipsum.txt");

const callback = (err, data) => {
  if (err) {
    console.error(err);
  } else {
    console.log(data);
  }
};

problem2(fileName, callback);
