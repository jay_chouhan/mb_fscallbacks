/*
    Problem 1:
    
    Using callbacks and the fs module's asynchronous functions, do the following:
        1. Create a directory of random JSON files
        2. Delete those files simultaneously 
*/

const fs = require("fs");

const problem1 = (directory, callback) => {
  fs.mkdir(directory, { recursive: true }, (err) => {
    if (err) {
      callback(new Error(`Error creating directory ${directory}`, null));
    } else {
      console.log(`${directory} created`);

      let randomNum = Math.floor(Math.random() * 20) + 1;

      for (let i = 0; i < randomNum; i++) {
        const fileName = "random" + i;

        fs.writeFile(`${directory}/${fileName}`, JSON.stringify({}), (err) => {
          if (err) {
            callback(new Error(`Error creating file ${fileName}`, null));

            i -= 1;
          } else {
            console.log(`${fileName} created in ${directory}`);

            fs.unlink(`${directory}/${fileName}`, (err) => {
              if (err) {
                callback(new Error(`Error deleting file ${fileName}`, null));

                i -= 1;
              } else {
                console.log(`${fileName} deleted succesffully`);
              }
            });
          }
        });
      }
    }
  });
};

module.exports = problem1;
